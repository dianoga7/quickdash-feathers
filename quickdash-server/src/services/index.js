const users = require('./users/users.service.js');
const devices = require('./devices/devices.service.js');
module.exports = function() {
	const app = this;
	app.configure(users);
	app.configure(devices);
};

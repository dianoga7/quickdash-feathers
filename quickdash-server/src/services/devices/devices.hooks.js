const { authenticate } = require('@feathersjs/authentication').hooks;

const processDevice = require('../../hooks/process-device');

module.exports = {
	before: {
		all: [authenticate('jwt')],
		find: [],
		get: [],
		create: [processDevice()],
		update: [processDevice()],
		patch: [processDevice()],
		remove: []
	},

	after: {
		all: [],
		find: [],
		get: [],
		create: [],
		update: [],
		patch: [],
		remove: []
	},

	error: {
		all: [],
		find: [],
		get: [],
		create: [],
		update: [],
		patch: [],
		remove: []
	}
};

// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = function(options = {}) {
	return async context => {
		const { data, method } = context;

		if (!data.id) {
			throw new Error('A device must have an ID');
		}

		const user = context.params.user;

		context.data = {
			data,
			userId: user._id
		};

		if (method == 'create') {
			context.data.createdAt = new Date().getTime();
		} else {
			context.data.updatedAt = new Date().getTime();
		}

		return context;
	};
};
